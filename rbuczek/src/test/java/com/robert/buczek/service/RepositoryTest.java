package com.robert.buczek.service;

import com.robert.buczek.domain.Loan;
import com.robert.buczek.repository.ILoanRepository;
import com.robert.buczek.repository.MockRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by robert on 13.02.2017.
 */
@RunWith(SpringRunner.class)
public class RepositoryTest {

    private ILoanRepository service;

    public RepositoryTest(){
        service = new MockRepository();
        service.save(new Loan(1, 32131,321313));
        service.save(new Loan(2, 32131,32131));
        service.save(new Loan(3, 43142134,432412));
        service.save(new Loan(4, 4134,32131));
    }

    @Test
    public void testListAllLoan() throws Exception {
        Assert.assertTrue(!service.findAll().isEmpty());
    }

    @Test
    public void testGetProductById() throws Exception {
    }

    @Test
    public void testSaveProduct() throws Exception {
        int len = service.findAll().size();
        service.save(new Loan(5, 4134,32131));
        Assert.assertNotEquals(len, service.findAll().size());
    }

    @Test
    public void testDeleteProduct() throws Exception {
        int len = service.findAll().size();
        List<Loan> list = service.findAll();
        for(Loan l : list)
            service.delete(l.getId());
        Assert.assertNotEquals(len, service.findAll().size());
    }
}