/json get all loan
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/app/loan
/xml get all loan
curl -i -H "Accept: application/json" -H "Content-Type: application/xml" -X GET http://localhost:8080/app/loan

/wyswietla szczegolowe informacje w wersji json
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/app/loan/{id}
example
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/app/loan/1

/wyswietla szczegolowe informacje w wersji xml
curl -i -H "Accept: application/json" -H "Content-Type: application/xml" -X GET http://localhost:8080/app/loan/{id}
example
curl -i -H "Accept: application/json" -H "Content-Type: application/xml" -X GET http://localhost:8080/app/loan/1


usuwanie o nr id
curl -X "DELETE" http://localhost:8080/app/loan/{id}
example
curl -X "DELETE" http://localhost:8080/app/loan/3

post example
curl -H "Content-Type: application/json" -X POST -d '{"id":"1500","loanAmount":"32","loanTermMonths":"0"}' http://localhost:8080/app/loan

update example
curl -H "Content-Type: application/json" -X PATCH -d '{"id":"1500","loanAmount":"32","loanTermMonths":"2000"}' http://localhost:8080/app/loan

