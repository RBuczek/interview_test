package com.robert.buczek.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by robert on 13.02.2017.
 */
@Component
public class RandomExtend implements IRandomExtend
{
    private Random random = new Random();

    public Integer generate(){
        return random.nextInt();
    }
}
