package com.robert.buczek.utils;

/**
 * Created by robert on 13.02.2017.
 */
public interface IRandomExtend
{
    Integer generate();
}
