package com.robert.buczek.controller;

import com.robert.buczek.domain.Loan;
import com.robert.buczek.service.ILoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by robert on 13.02.2017.
 */
@Controller
@RequestMapping(value = {"/app","/"})
public class IndexController
{
    @Autowired
    ILoanService service;

    @RequestMapping(value = {"/loan"})
    public @ResponseBody List<Loan> mainPage2(Model model)
    {
        model.addAttribute("loan", service.listAllLoan());
        return  service.listAllLoan();
    }

    @RequestMapping("loan/{id}")
    public @ResponseBody Loan showProductJSON(@PathVariable Integer id) {
        return service.getProductById(id);
    }

    @PostMapping("/loan")
    public ResponseEntity<?> createNewLoan(@RequestBody Loan loan)
    {
        service.saveProduct(loan);
        return new ResponseEntity("Successfully add", new HttpHeaders(), HttpStatus.OK);
    }

    @PatchMapping("/loan")
    public ResponseEntity<?> update(@RequestBody Loan loan)
    {
        service.saveProduct(loan);
        return new ResponseEntity("Update loan", new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("loan/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id)
    {
        service.deleteProduct(id);
        return new ResponseEntity("Delete success", new HttpHeaders(), HttpStatus.OK);
    }

}
