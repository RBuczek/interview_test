package com.robert.buczek.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by robert on 13.02.2017.
 */
@Entity
@XmlRootElement
public class Loan{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private Integer loanAmount;
    private Integer loanTermMonths;

    public Loan() {
    }

    public Loan(Integer loanAmount, Integer loanTermMonths) {
        this.loanAmount = loanAmount;
        this.loanTermMonths = loanTermMonths;
    }

    public Loan(Integer id,Integer loanAmount, Integer loanTermMonths) {
        this.id = id;
        this.loanAmount = loanAmount;
        this.loanTermMonths = loanTermMonths;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Integer getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Integer loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanTermMonths() {
        return loanTermMonths;
    }

    public void setLoanTermMonths(Integer loanTermMonths) {
        this.loanTermMonths = loanTermMonths;
    }

    public void update(Loan loan){
        this.id = loan.getId();
        this.loanAmount = loan.getLoanAmount();
        this.loanTermMonths = loan.getLoanTermMonths();
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", loanAmount=" + loanAmount +
                ", loanTermMonths=" + loanTermMonths +
                '}';
    }
}
