package com.robert.buczek.service;

import com.robert.buczek.domain.Loan;

import java.util.List;

/**
 * Created by robert on 13.02.2017.
 */
public interface ILoanService {

    List<Loan> listAllLoan();

    Loan getProductById(Integer id);

    Loan saveProduct(Loan product);

    void deleteProduct(Integer id);
}
