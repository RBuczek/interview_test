package com.robert.buczek.service;

import com.robert.buczek.domain.Loan;
import com.robert.buczek.repository.ILoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by robert on 13.02.2017.
 */
@Service
public class LoanService implements ILoanService {

    @Autowired
    ILoanRepository repository;

    @Override
    public List<Loan> listAllLoan() {
        return repository.findAll();
    }

    @Override
    public Loan getProductById(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public Loan saveProduct(Loan product) {
        return repository.save(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        repository.delete(id);
    }
}
