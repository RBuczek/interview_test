package com.robert.buczek.repository;

import com.robert.buczek.domain.Loan;

import java.util.List;

/**
 * Created by robert on 13.02.2017.
 */
public interface ILoanRepository{

    List<Loan> findAll();

    Loan findOne(Integer id);

    Loan save(Loan product);

    void delete(Integer id);

}
