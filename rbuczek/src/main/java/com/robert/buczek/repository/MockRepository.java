package com.robert.buczek.repository;

import com.robert.buczek.domain.Loan;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robert on 13.02.2017.
 */
@Repository
public class MockRepository implements ILoanRepository
{
    private static List<Loan> data = new ArrayList<>();

    @Override
    public List<Loan> findAll()
    {
        return data;
    }

    @Override
    public Loan findOne(Integer id)
    {
        for (Loan l : data)
            if(l.getId() == id)
                return l;

        return null;
    }

    @Override
    public Loan save(Loan product)
    {
        for(Loan l : data)
            if (l.getId() == product.getId()){
                l.update(product);
                return product;
            }
        data.add(product);
        return product;
    }

    @Override
    public void delete(Integer id)
    {
        try {
            data.stream().filter(l -> l.getId() == id).forEach(l -> data.remove(l));
        }catch (Exception e){

        }
    }
}
