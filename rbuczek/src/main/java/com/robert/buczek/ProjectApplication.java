package com.robert.buczek;

import com.robert.buczek.domain.Loan;
import com.robert.buczek.repository.ILoanRepository;
import com.robert.buczek.service.ILoanService;
import com.robert.buczek.utils.IRandomExtend;
import com.robert.buczek.utils.RandomExtend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProjectApplication {

	IRandomExtend random;

	@Autowired
	public ProjectApplication(IRandomExtend random){
		this.random = random;
	}

	private static final Logger log = LoggerFactory.getLogger(ProjectApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

	@Bean
	public CommandLineRunner init(ILoanService repository) {
		return (args) -> {
			repository.saveProduct(new Loan(1, 32131,321313));
			repository.saveProduct(new Loan(2, 32131,32131));
			repository.saveProduct(new Loan(3, 43142134,432412));
			repository.saveProduct(new Loan(4, 4134,32131));

			for (int i = 5; i < 15; i++)
				repository.saveProduct(new Loan(i, random.generate(),random.generate()));

			log.info("Loan found with findAll():");
			log.info("-------------------------------");
			for (Loan customer : repository.listAllLoan())
				log.info(customer.toString());
			log.info("");
		};
	}
}
